package com.zuitt.discussion.services;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService{

    @Autowired
    private PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    @Override
    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();

        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        postRepository.save(newPost);
    }

    @Override
    public Iterable<Post> getPosts() {

        return postRepository.findAll();
    }

    @Override
    public ResponseEntity updatePost(Long id, Post post) {
        Post postForUpdate = postRepository.findById(id).get();

        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully.", HttpStatus.OK);
    }
}
