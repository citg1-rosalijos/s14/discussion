package com.zuitt.discussion.config;

import com.zuitt.discussion.services.JwtUserDetailsService;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService; // responsible for retrieve the user from the database

    private final JwtToken jwtTokenUtil;

    // this constructor assigns the JwtToken instance to the jwtTokenUtil
    // this is known as dependency injection
    // provided instance of its required dependency from the outside
    public JwtRequestFilter(JwtToken jwtTokenUitl) {
        this.jwtTokenUtil = jwtTokenUitl; // allows using methods from JwtToken
    }

    @Override
    // gets called for every incoming HTTP request
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        // gets the JWT token from the Authorization header of the incoming request
        // and attempts to validate it using the JwtToken utility class.
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = null;
        String jwtToken = null;

        if(requestTokenHeader != null) {
            // if a token is seen in the header it will be placed in the jwtToken variable
            // will be used to extract the username from the token claims.
            jwtToken = requestTokenHeader;
            try {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token.");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired.");
            }
        } else {
            logger.warn("JWT Token is incomplete.");
        }

        if(username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);

            if(jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );

                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                // This allows the current authenticated user to be retrieved and authorized on the other parts of the application.
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }

        // This is a method call that is typically used in a servlet filter to pass the request and response objects to the next filter in the filter chain or next servlet that will handle the request.
        filterChain.doFilter(request, response);
    }
}
